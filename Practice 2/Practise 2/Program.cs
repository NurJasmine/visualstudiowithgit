﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Odd numbers from 1 to 20. Prints each number per line.");
            for (int n = 1; n < 20; n++)
            {
                if (n % 2 != 0)
                {
                    Console.WriteLine(n.ToString());
                    Console.ReadLine();
                }
            }
        }
    }
}
