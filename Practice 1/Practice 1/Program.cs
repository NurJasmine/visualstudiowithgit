﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int number1, number2;

            Console.Write("Enter a number: ");
            number1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter another number: ");
            number2 = Convert.ToInt32(Console.ReadLine());

            int sum = number1 + number2;
            Console.WriteLine("the sum of the 2 numbers is : ", sum);
        }
    }
}
